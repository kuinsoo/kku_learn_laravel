@extends('users.layout')
    @section('title', 'Detail Page')
    @section('content')

    <input type="button" value="돌아가기" onclick="javascript:history.back();">

    <div>
        <h2>상세보기 페이지</h2>
        <p>Name : {{ $user->name }}</p>
        <p>Name : {{ mb_substr($user->password, 0, -3)."***" }}</p>
        <p>Name : {{ $user->email }}</p>
    </div>
    <br/>
    <div>
        <form action="" method="GET">
            <input type="submit" value="수정" formaction="/users/{{ $user->id }}/edit"> &Tab;
        </div>
        @csrf
    </form>
    <form action="" method="POST">
        @method("DELETE")
            <input type="submit" value="삭제" formaction="/users/{{ $user->id }}">
            @csrf
    </form>

    @endsection
