<div>
    <label for="userName">이름</label>
    <input type="text" id="userName" name="name" value="{{ old('name') ?? $user->name }}" />
    @error('name') <p style='color:red;'>{{ $message }}</p> @enderror
</div>
<div>
    <label for="userEmail">이메일</label>
    <input type="email" id="userEmail" name="email" value="{{ old('email') ?? $user->email }}"/>
    @error('email') <p style='color:red;'>{{ $message }}</p> @enderror
</div>
<div>
    <label for="userEmail_verified">이메일확인</label>
    <input type="email" id="userEmailVerified" name="email_verified_at" value="{{ old('email_verified_at') ?? $user->email }}"/>
    @error('email_verified_at') <p style='color:red;'>{{ $message }}</p> @enderror
</div>
<div>
    <label for="userPassword">비밀번호</label>
    <input type="password" id="userPassword" name="password" value="{{ old('password') ?? $user->password }}"/>
    @error('password') <p style='color:red;'>{{ $message }}</p> @enderror
</div>

@csrf
