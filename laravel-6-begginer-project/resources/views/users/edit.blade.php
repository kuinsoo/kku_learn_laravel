@extends('users.layout')

    @section('content')
    <form action="/users/{{ $user->id }}" method="POST">
        @method("PATCH")
        @include('users.form')
        <button>저장</button>
    </form>
    @endsection
