<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// 간단한 연결  closer
Route::get('/hello/hello', function () {
    return 'Hello';
});

// View => 간단한 view page 연결   ( 폴더 하위 연결시 / 대시 . 사용가능 )
Route::get('/hello', function () {
    return view("subviews.hello");
});

// Pass Data to View => 간단한 view page 에 데이터 전달
Route::get('/hello', function () {
    $variable = 'Hello from Routes.'; // 간단한 문자열 전달

    // return view("subviews.hello", [ // 파라미터 view, data 배열, mergData 배열
    //     'someData' => $variable, // key => value  위에 문자열 데이터를 넘겨주기
    // ]);

    return view("subviews.hello", compact('variable')); // compact 방식으로 전달 가능 변수명과 같아야한다.  key 가 곧 변수명
});

//controller 이용
Route::get('/helloController', 'HelloController@index');


/* epic */

//e5 Controller
Route::view('epic/e5', 'epic/e5_Controller');

//e6 Blade Templates
Route::get('epic/e6', 'AppController@index');
Route::get('epic/e6_about', 'AppController@about');
Route::get('epic/e6_service', 'AppController@services');

// controller 를 이용하지 않고 짧게 가능
// Route::view('epic/e6_about', 'epic.e6_About');
// Route::view('epic/e6_service', 'epic.e6_Services');


//e7 MySQL database
Route::get('service', 'ServiceController@index');
Route::post('service', 'ServiceController@store');


//e9 ReSTful Contoller
Route::get('/customers', 'CustomerController@index');
Route::get('/customers/create', 'CustomerController@create');
Route::post('/customers', 'CustomerController@store');
Route::get('/customers/{customer}', 'CustomerController@show');
Route::get('/customers/{customer}/edit', 'CustomerController@edit');
Route::patch('/customers/{customer}', 'CustomerController@update');
Route::delete('/customers/{customer}', 'CustomerController@destory');
