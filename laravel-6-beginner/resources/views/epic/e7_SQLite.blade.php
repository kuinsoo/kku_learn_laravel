<h1>SQLite</h1>

<pre>
    - SQLite 설치

    - database 폴더 아래 생성
        database.sqlite

    - .env 파일 수정
        DB_CONNECTION=sqlite

    - tinker 접속 후 $service 생성
        php artisan tinker

        $service = new \App\Service();
        echo $service;
        []

        $service->name = "Hello from SQLITE";
        echo $service;
        {"name":"Hello from SQLITE"}

        $service->sava();
        => true;

        exit

</pre>
