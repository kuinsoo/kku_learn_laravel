### e9_RESTful_Controllers Part1 

```php
    - route 등록
    Route::get('customers', 'CustomerController@index'); // 기본 인덱스
    Route::get('customers/create', 'CustomerController@create'); // 생성입력 페이지
    Route::post('customers', 'CustomerController@store'); // 생성 연결

    - php artisan make:model Customer -cm 
        controller 와 migration 함꼐 생성

    - migration 컬럼 추가 
        $table->string('name');
        $table->string('email');
        $table->text('notes')->nullable(); // 널 허용

    - Customer model 컬럼 등록
        protected $guarded = [];
    
    - CutomerController 등록
        public function index() {
            $customers = \App\Customer::all();

            return view('customer.index', compact('customers'));
       }

        public function create() {
            return view('customer.create');
        }

            public function store() {

            \App\Customer::create(request()->validate([
            'name'=>'required',
            'email'=>'required|email'
        ]));

            return redirect('/customers');
        }
    
    - Customer Index 출력화면
        @forelse ($customers as $customer)
            <p><strong>{{ $customer->name }}</strong>({{ $customer->email }})</p>
        @empty
            <p>No Customers to show</p>
        @endforelse

    - Customer Create 화면 
    <form action="/customers" method="POST">
        <div>
            <label for="name">Name</label>
            <input type="text" name="name" autocomplete="off" value="{{ old('name') }}"> // old 폼 지움 방지
            @error('name') <p style="color:red;">{{ $message }}</p>@enderror // error 검색
        </div>
        <div>
            <label for="email">Email</label>
            <input type="text" name="email" autocomplete="off" value="{{ old('email') }}">
            @error('email') <p style="color:red;">{{ $message }}</p>@enderror
        </div>
        @csrf
        <button>Add New Customer</button>
    </form>
```
Actions Handled By Resource Controller
    Verb        URI	                    Action	    Route Name
    GET         /photos	                index	    photos.index
    GET	        /photos/create	        create	    photos.create
    POST	    /photos	                store	    photos.store
    GET	        /photos/{photo}	        show	    photos.show
    GET	        /photos/{photo}/edit	edit	    photos.edit
    PUT/PATCH	/photos/{photo}	        update	    photos.update
    DELETE	    /photos/{photo}	        destroy	    photos.destroy
