### Form Validation

<pre>
    - Route 연결
        Route::get('service', 'ServiceController@index');
        Route::post('service', 'ServiceController@store');

    - Service Controller
        // 단순하게 get 방식 접근 
        function index() {
            $services = \App\Service::all();
            return view('service.index', compact('services'));
        }

        // post 방식 현재페이지로 리턴
        function store() {
        
        // validate rules 정보 => https://laravel.com/docs/6.x/validation#available-validation-rules
        
        // validata 세팅 
        // $data = request()->validate([
        //     'name' => 'required|min:5'
        // ]);

        // dd($data); // name => data

        // 아래 3줄을 줄여서
        // \App\Service::create($data);

        // service 등록
        // $service = new \App\Service();
        // $service->name = request('name');
        // $service->save();

        // 인라인 스타일 위에 코드를 한번에 다 줄여서 
        \App\Service::create(request()->validate([
            'name' => 'required|min:5'
        ]));

       return redirect()->back();  // 페이지 리로드
    }

</pre>
