<h1>controller</h1>

<pre>
    폴더이동

    - help 이용
        php artisan help make:controller
    - Controller 생성
        php artisan make:controller HelloController

    - Route 이동
        Route::get('/url', '컨트롤러 이름@함수이름');
</pre>
