<h1>MySQL</h1>

<pre>
    - MySQL 설치 ( 설치버전으로 진행 ))
        https://downloads.mariadb.org/mariadb/10.4.8/#bits=64&os_group=windows

        https://javaplant.tistory.com/31  (참조)

    - 데이터 베이스 생성 및 접속
        . 접속
        > mysql -u root -p

        . 데이터베이스 생성
        > create database laravel

        . 권한
        > grant all privileges on laravel.* to '유저이름'@'localhost' identified by '설정패스워드';

        . 적용
        > flush privileges;

    - .env 파일 수정
        DB_CONNECTION=mysql
        DB_HOST=127.0.0.1
        DB_PORT=3306
        DB_DATABASE=laravel
        DB_USERNAME=유저아이디 기본 root
        DB_PASSWORD=유저 비밀번호

    - Model & Controller 생성 ( singular )
        php artisan make:model Service

    - Controller 로직 변경
        function services() {

            $services = \App\Service::all();
            // dd($services);

            return view('epic.e7_MySQL', compact('services'));
        }

    - Migration 생성 ( plular )
        php artisan make:migration creates_services_table --create services

    - Migration 컬럼추가
        public function up()
        {
            Schema::create('services', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->timestamps();
            });
        }

    - Migration 실행
        php artisan migrate

    - List 에 데이터 베이스 값 출력
        &#60;ul&#62;
        &#64;forelse ($services as $service)
            &#60;li&#62;&#123;&#123; $service->name &#125;&#125;&#60;/li&#62;
        &#64;empty
            &#60;li&#62;No service avariable.&#60;/li&#62;
        &#64;endforelse
        &#60;/ul&#62;

</pre>


@extends('epic.e7_BladeTemplates')

@section('title', 'Service Page')
@section('content')
    <h1>Welcome to Laravel 6 from Service</h1>

    <ul>
        @forelse ($services as $service)
            <li>{{ $service->name }}</li>
        @empty
            <li>No service avariable.</li>
        @endforelse
    </ul>
@endsection
