<h1>Blade Templates</h1>

<pre>
    폴더이동

    - Controller 생성
        php artisan make:controller AppController

    - Route 이동
        Route::get('epic/e6', 'AppController@index');
        Route::get('epic/e6_about', 'AppController@about');
        Route::get('epic/e6_service', 'AppController@service');

    - About  페이지 생성
        &#64;extends('epic.e6_BladeTemplates')

        &#64;section('title', 'About Us Page')
        &#64;section('content')
            &#60;h1&#62;Welcome to Laravel 6 from About&#60;/h1&#62;

            &#60;p&#62;With Some additional text.&#60;/p&#62;
        &#64;endsection

    - Service  페이지 생성
        &#64;extends('epic.e6_BladeTemplates')

        &#64;section('title', 'Service')
        &#64;section('content')
            &#60;h1&#62;Welcome to Laravel 6 from Service&#60;/h1&#62;

            &#60;ul&#62;
                &#60;li&#62;Service 1
                &#60;li&#62;Service 2&#60;/li&#62;
                &#60;li&#62;Service 3&#60;/li&#62;
            &#60;/ul&#62;
        &#64;endsection

    - Controller 세팅
        function index() {
            return view('epic.e6_BladeTemplates');
        }

        function about() {
            return view('epic.e6_About');
        }

        function services() {
            $services = [
                'Service 1',
                'Service 2',
                'Service 3',
                'Service 4',
            ];
            return view('epic.e6_Service', compact('services'));
        }

    - Service list foreach | forelse 사용
        > foreach
        &#60;ul&#62;
            &#64;foreach ($services as $service)
                 &#60;li&#62;&#123;&#123; $service &#125;&#125;&#60;/li&#62;
            &#64;endforeach
        &#60;/ul&#62;

        > forelse   빈객체를 확인 가능
        &#60;ul&#62;
            &#64;forelse ($services as $service)
                 &#60;li&#62;&#123;&#123; $service->$name &#125;&#125;&#60;/li&#62;
            &#64;empty
                 &#60;li&#62;No service avariable.&#60;/li&#62;
            &#64;endforelse
        &#60;/ul&#62;
</pre>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title', 'Blade templates')</title>
</head>
<body>
    @include('epic.e6_Nav')
    @yield('content')

</body>
</html>

