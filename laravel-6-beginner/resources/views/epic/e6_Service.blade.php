@extends('epic.e6_BladeTemplates')

@section('title', 'Service Page')
@section('content')
    <h1>Welcome to Laravel 6 from Service</h1>

    <ul>
        @foreach ($services as $service)
        <li>{{ $service }}</li>
        @endforeach

        @forelse ($services as $service)
            <li>{{ $service }}</li>
        @empty
            <li>No service avariable.</li>
        @endforelse
    </ul>
@endsection
