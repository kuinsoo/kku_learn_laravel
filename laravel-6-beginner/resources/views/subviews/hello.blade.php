<h1>Hello!</h1>

<p>Learning Laravel 6</p>

<hr/>
<h3>기본적인 방법!</h3>
<pre>
    return view("subviews.hello", [
        'someData' => $variable,
    ]);
</pre>
<br/>
<h3>compact 방방식으로 받을때</h3>
<p>key 와 variable 일치하여야 한다.</p>
<p>return view("subviews.hello", compact('variable'));</p>
<hr/>
<h1>Data 받기 : {{ $variable }}</h1>
<h3>php 방식</h3> {{-- original php 방식 --}}
<p>&lt;?php echo $someData?&gt;<p>
<h3>blade 방식</h3> {{-- blade 방식 --}}
<p>	&#123;&#123; $someData &#125;&#125;<p>

