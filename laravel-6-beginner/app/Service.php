<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    // 모든 필드 입력
    // protected $fillable = ['name'];

    // 모든 필드 자동 사용
    protected $guarded = [];
}
