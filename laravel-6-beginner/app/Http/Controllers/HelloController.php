<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelloController extends Controller
{
    //index hello
    function index() {
        $variable = 'Hello from Controller.'; // 간단한 문자열 전달

        // return view("subviews.hello", [ // 파라미터 view, data 배열, mergData 배열
        //     'someData' => $variable, // key => value  위에 문자열 데이터를 넘겨주기
        // ]);

        return view("subviews.hello", compact('variable'));
    }
}
