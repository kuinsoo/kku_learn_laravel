<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppController extends Controller
{
    // index
    function index() {
        return view('epic.e6_BladeTemplates');
    }

    function about() {
        return view('epic.e6_About');
    }

    function services() {

        $services = [
            'Service 1',
            'Service 2',
            'Service 3',
            'Service 4',
        ];

        return view('epic.e6_Service', compact('services'));
    }
}
