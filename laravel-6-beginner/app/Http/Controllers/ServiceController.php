<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServiceController extends Controller
{
    //
    function index() {

        // Model - singular
        // Service

        // Table - plular
        // SErivces

        $services = \App\Service::all();
        // dd($services);

        return view('service.index', compact('services'));
    }

    function store() {
        // validate rules => https://laravel.com/docs/6.x/validation#available-validation-rules
        // $data = request()->validate([
        //     'name' => 'required|min:5'
        // ]);

        // dd($data); // name => data

        // 아래 3줄을 줄여서
        // \App\Service::create($data);

        // $service = new \App\Service();
        // $service->name = request('name');
        // $service->save();


        // 인라인 스타일
        \App\Service::create(request()->validate([
            'name' => 'required|min:5'
        ]));


       return redirect()->back();
    }
}
