<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;  // Customer 로 사용가능 

class CustomerController extends Controller
{
    public function index() {
        $customers = Customer::all();

        return view('customer.index', compact('customers'));
    }

    public function create() {
        $customer = new Customer();
        return view('customer.create', compact('customer'));
    }

    public function store() {

        // Customer::create(request()->validate([
        //     'name'=>'required',
        //     'email'=>'required|email'
        // ]));
        $customer = Customer::create($this->validatedData());
        return redirect('/customers/'.$customer->id);
    }

    public function show(Customer $customer) {
        // $customer = Customer::findOrFail($customerId);
        // return view('customer.show', compact('customer'));
        return view('customer.show', compact('customer'));
    }

    public function edit(Customer $customer) {
        return view('customer.edit', compact('customer'));
    }

    public function update(Customer $customer) {
        
        // $data = request()->validate([
        //     'name'=>'required',
        //     'email'=>'required|email'
        // ]);

        $customer->update($this->validatedData());

        return redirect('/customers');
    }

    public function destory(Customer $customer){
        $customer->delete($customer);
        return redirect('/customers');

    }

    // validate 사용을 좀더 멋지게
    protected function validatedData() {
        return request()->validate([
            'name'=>'required',
            'email'=>'required|email'
        ]);
    }

}
