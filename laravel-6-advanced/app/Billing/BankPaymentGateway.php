<?

namespace App\Billing;

use Illuminate\Support\Str;

class BankPaymentGateway
{
    public function charge($amount)
    {
        // charge the bank

        return [
            'amount' => $amount,
            'confirmation_number' => Str::random(),
        ];
    }
}
